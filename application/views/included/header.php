<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= APP_NAME ?> - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/admin') ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/admin') ?>/css/font.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/admin') ?>/css/sb-admin-2.min.css" rel="stylesheet">
    <?php ($css != '') ? $this->load->view($css) : ''; ?>

</head>

<body id="page-top">