<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url(); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fab fa-accusoft"></i>
        </div>
        <div class=" sidebar-brand-text mx-3">Simelda <sup>v.1</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Administrator
    </div>

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('dashboard'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <?php if ($_SESSION['role_id'] == '1') : ?>
        <!-- ROLE SUPER ADMIN -->
        <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            Master
        </div>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('master') ?>">
                <i class="fas fa-fw fa-cog"></i>
                <span>Settings</span></a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-fw fa-user"></i>
                <span>Logout</span></a>
        </li>
        <!-- END ROLE SUPER ADMIN -->
    <?php endif; ?>

    <?php if ($_SESSION['role_id'] == '2') : ?>
        <!-- ROLE AGENT -->
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            User
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('coaching'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Coaching</span></a>
        </li>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('konseling'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Konseling</span></a>
        </li>
        <!-- END ROLE AGENT -->
    <?php endif; ?>

    <?php if ($_SESSION['role_id'] == '3') : ?>
        <!-- ROLE TL -->
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Pembinaan
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('coaching'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Coaching</span></a>
        </li>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('konseling'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Konseling</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('batl'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>BATL</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('sp'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>SP</span></a>
        </li>
        <!-- END ROLE TL -->
    <?php endif; ?>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->