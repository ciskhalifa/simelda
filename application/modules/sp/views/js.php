<!-- Latest compiled and minified JavaScript -->
<script src="<?=base_url()?>assets/admin/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/js/validator.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/vendor/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/admin/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/js/jszip.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/js/pdfmake.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/js/vfs_fonts.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?=base_url('assets/admin')?>/vendor/select2/select2.min.js"></script>

<div id="myConfirm" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan menghapus data <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($_SESSION['role_id'] == '3'): ?>
    <div id="updateBina" class="modal animated--grow-in">
        <div class="modal-success">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Pembinaan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">

                            <div class="form-group row">
                                <label class="col-md-4 label-control">Agent</label>
                                <div class="col-md-8">
                                    <select class="select2 form-control agent" name="kode_agent" id="agent" style="width:100%" disabled>
                                        <option value="">- Pilihan -</option>
                                        <?php
                                        $n = (isset($arey)) ? $arey['kode_agent'] : '';
                                        $q = $this->Data_model->selectData('m_users', 'kode', array('role_id' => 2));
                                        foreach ($q as $row) {
                                            $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                            echo '<option data-id="' . $row->kode . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->fullname . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Tanggal</label>
                                <div class="col-md-8">
                                    <input type="date" class="form-control input-sm tgl_kejadian" name="tgl_kejadian" id="tgl_kejadian" value="" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                            <label class="col-md-4 label-control">Jenis SP</label>
                                <div class="col-md-8">
                                    <select class="select2 form-control kode_kts" name="kode_kts" id="kode_kts" style="width:100%" required>
                                        <option value="">- Pilihan -</option>
                                        <?php
                                            $n = (isset($arey)) ? $arey['kode_kts'] : '';
                                            $q = $this->Data_model->selectData('m_kts_subkategori', 'kode', array('jenis_bina' => '3'));
                                            foreach ($q as $row) {
                                                $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                                echo '<option data-num="' . $row->kts_verifikasi_num . '" data-str="' . $row->kts_verifikasi_str . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->kts_subkategori . '</option>';
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Waktu Verifikasi</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control input-sm waktu_verifikasi" placeholder="Waktu Verifikasi" name="waktu_verifikasi" disabled>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Tanggal Verifikasi</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control input-sm tgl_verifikasi" placeholder="Tanggal Verifikasi" name="tgl_verifikasi" readonly>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Kronologis</label>
                                <div class="col-md-8">
                                    <textarea class="form-control input-sm masalah" placeholder="Teguran Lisan" name="masalah" id="masalah" value="" required></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Pertimbangan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control input-sm rekomendasi" placeholder="Verifikasi" name="rekomendasi" id="rekomendasi" value=""></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Tindakan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control input-sm action_plan" placeholder="Action Plan" name="action_plan" id="action_plan" value=""></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Status</label>
                                <div class="col-md-8">
                                    <select class="select2 form-control status" name="status" id="status" style="width:100%" >
                                        <option value="">- Pilihan -</option>
                                        <option value="VERIFIKASI">VERIFIKASI</option>
                                        <option value="SELESAI">SELESAI</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary updatePembinaan">Update</button>
                                <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
<?php endif;?>

    <script>
        $(function() {
            $("#AntrianTable").dataTable();
            $('#openform').on('click', function() {
                $('#modalForm').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
            $('#modalForm').on('hidden.bs.modal', function() {
                $(this).find('form')[0].reset();
                if ($("#pesanan tbody tr").length > 1) {
                    $("#pesanan tbody tr:last-child").remove();
                }
            });
            $('.tgl_kejadian').on('change', function(){
                var date = new Date($(this).val());
                var date2 = new Date(date.setDate(date.getDate() + parseInt(1)));
                var format = date2.getFullYear() + "-" + date2.getMonth() + "-" + date2.getDate();
                 $(".tgl_bina").val(format);
            });
            $(".kode_kts").on('change', function() {
                var date1 = new Date($('.tgl_kejadian').val());
                var date2 = new Date(date1.setDate(date1.getDate() + parseInt($(this).find(':selected').attr('data-num'))));
                var format = date2.getFullYear() + "-" + date2.getMonth() + "-" + date2.getDate();
                $(".waktu_verifikasi").val($(this).find(':selected').attr('data-str'));
                $(".tgl_verifikasi").val(format);
            });
            $(".edit").on('click', function() {
                $("#updateActionPlan").modal();
                $("#updateBina").modal();
                $.ajax({
                    url: "<?=base_url('sp/getAgent')?>",
                    type: "POST",
                    data: "kode=" + $(this).data('kode'),
                    dataType: "json",
                    success: function(html) {
                        $("#kode").val(html[0].kode);
                        $(".agent").find("option[value=" + html[0].kode_agent + "]").prop('selected', true);
                        $(".tgl_bina").val(html[0].tgl_bina);
                        $(".tgl_kejadian").val(html[0].tgl_kejadian);
                        $(".kode_kts").find("option[value=" + html[0].kode_kts + "]").prop('selected', true);
                        $(".waktu_verifikasi").val(html[0].kts_verifikasi_str);
                        $(".tgl_verifikasi").val(html[0].tgl_verifikasi);
                        $(".masalah").text(html[0].masalah);
                        $(".rekomendasi").text(html[0].rekomendasi);
                        $(".action_plan").text(html[0].action_plan);
                    }
                })
            })
            $(".delete").on('click', function() {
                $("#myConfirm").modal();
                $(".lblModal").text($(this).data('kode'));
                $("#cid").val($(this).data('kode'));
                $("#getto").val("<?=base_url('sp/hapus')?>");
                $("#cod").val("sp");
            })
            $(".detail").on('click', function() {
                $('#tabelpegawai').hide();
                $('#contentdetail').load('' + 'sp/view_detail/' + $(this).attr("data-kode"));
                $('#containerdetail').fadeIn('fast');
            });
            $('.kecamatan').select2();

            $("#btnYes").bind("click", function() {
                var link = $("#getto").val();
                $.ajax({
                    url: link,
                    type: "POST",
                    data: "cid=" + $("#cid").val() + "&cod=" + $("#cod").val(),
                    dataType: "html",
                    beforeSend: function() {
                        if (link != "#") {}
                    },
                    success: function(html) {
                        notify("Delete berhasil", "danger")
                        $("#myConfirm").modal("hide")
                        location.reload(true);
                    }
                })
            });

            $('.simpanActionplan').bind("click", function(){
                $.ajax({
                    url: "<?=base_url('sp/updateActionPlan');?>",
                    type: "POST",
                    data: "kode=" + $("#kode").val() +"&action_plan=" + $("#action_plan").val(),
                    dataType: "html",
                    success: function(html) {
                        notify("Update berhasil", "success")
                        $("#updateActionPlan").modal("hide")
                        location.reload(true);
                    }
                })
            });

            $('.updatePembinaan').bind("click", function(){
                $.ajax({
                    url: "<?=base_url('sp/updateBina');?>",
                    type: "POST",
                    data: "kode=" + $("#kode").val() +"&status=" + $("#status").find(':selected').val(),
                    dataType: "html",
                    success: function(html) {
                        notify("Update berhasil", "success")
                        $("#updateBina").modal("hide");
                        location.reload(true);
                    }
                })
            });

        });
    </script>