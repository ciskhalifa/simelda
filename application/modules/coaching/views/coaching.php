<div class="container-fluid">
    <div id="tabelpegawai" class="slideInDown animated--grow-in" data-appear="appear" data-animation="slideInDown">
        <div class="content-detached">
            <div class="content-body">
                <section class="row">
                    <div class="col-md-12">
                        <div class="card shadow mb-2">
                            <div class="card-header py-3">
                                <h4 class="m-0 font-weight-bold text-primary"> List <?=$this->uri->segment('1');?></h4>
                                <a class="heading-elements-toggle"><i class="icon-arrow-right-4"></i></a>
                                <?php if ($_SESSION['role_id'] == '3'): ?>
                                    <div class="box-tools pull-right">
                                        <a href="javascript:;" class="btn btn-primary btn-sm" id="openform" data-tab="" data-original-title="Tambah Data" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                <?php endif;?>
                            </div>
                            <div class="card-body">
                                <table id="AntrianTable" class="table table-white-space table-bordered ">
                                    <thead>
                                        <tr>
                                            <?php
if ($kolom) {
    foreach ($kolom as $key => $value) {
        if (strlen($value) == 0) {
            echo '<th data-type="numeric"></th>';
        } else {
            echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
        }
    }
}
?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowdata as $row): ?>
                                            <tr>
                                                <td><?=$row->kode;?></td>
                                                <td><?=$row->agent;?></td>
                                                <td><?=$row->jenis_bina_desc;?></td>
                                                <td><?=$row->masalah;?></td>
                                                <td><?=$row->tgl_kejadian;?></td>
                                                <td><?=$row->tgl_bina;?></td>
                                                <td><?=$row->tgl_verifikasi;?></td>
                                                <td><?=$row->status;?></td>
                                                <?php if ($_SESSION['role_id'] == '2'): ?>
                                                    <td>
                                                        <button type="button" data-kode="<?=$row->kode?>" class="btn btn-sm btn-info detail" id="detail"><i class="fa fa-eye"></i></button>
                                                        <button type="button" data-kode="<?=$row->kode?>" class="btn btn-sm btn-warning edit" id="edit"><i class="far fa-edit"></i></button>
                                                    </td>
                                                <?php elseif ($_SESSION['role_id'] == '3'): ?>
                                                    <td>
                                                        <button type="button" data-kode="<?=$row->kode?>" class="btn btn-sm btn-info detail" id="detail"><i class="fa fa-eye"></i></button>
                                                        <button type="button" data-kode="<?=$row->kode?>" class="btn btn-sm btn-warning edit" id="edit"><i class="far fa-edit"></i></button>
                                                        <button type="button" data-kode="<?=$row->kode?>" class="btn btn-sm btn-danger delete" id="delete"><i class="fa fa-trash"></i></button>
                                                    </td>
                                                <?php endif;?>
                                            </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div id="containerdetail" class="content-detached">
        <div class="content-body">
            <div id="contentdetail">
            </div>
        </div>
    </div>

    <div class="modal fade text-xs-left animated--grow-in" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form role="form" id="sendForm" enctype="multipart/form-data" class="form-horizontal" action="<?=base_url('coaching/simpanData');?>" method="POST">
                    <input type="hidden" name="jenis_bina" value="1">
                    <div class="modal-header">
                        <h4 class="modal-title">Form Coaching</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Agent</label>
                            <div class="col-md-8">
                                <select class="select2 form-control agent" name="kode_agent" id="agent" style="width:100%">
                                    <option value="">- Pilihan -</option>
                                    <?php
$n = (isset($arey)) ? $arey['kode_agent'] : '';
$q = $this->Data_model->selectData('m_users', 'kode', array('role_id' => 2));
foreach ($q as $row) {
    $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
    echo '<option data-id="' . $row->kode . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->fullname . '</option>';
}
?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Tanggal Kejadian</label>
                            <div class="col-md-8">
                                <input type="date" class="form-control input-sm tgl_kejadian" name="tgl_kejadian" id="tgl_kejadian" value="" data-error="wajib diisi" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Ketidaksesuaian</label>
                            <div class="col-md-8">
                                <select class="select2 form-control kode_kts" name="kode_kts" id="kode_kts" style="width:100%">
                                    <option value="">- Pilihan -</option>
                                    <?php
$n = (isset($arey)) ? $arey['kode_kts'] : '';
$q = $this->Data_model->selectData('m_kts_subkategori', 'kode', array('jenis_bina' => '1'));
foreach ($q as $row) {
    $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
    echo '<option data-num="' . $row->kts_verifikasi_num . '" data-str="' . $row->kts_verifikasi_str . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->kts_subkategori . '</option>';
}
?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Tanggal Bina</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm tgl_bina" name="tgl_bina" id="tgl_bina" value="" data-error="wajib diisi" required readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Waktu Verifikasi</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm waktu_verifikasi" placeholder="Waktu Verifikasi" name="waktu_verifikasi" disabled>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Tanggal Verifikasi</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm tgl_verifikasi" placeholder="Tanggal Verifikasi" name="tgl_verifikasi" readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Permasalahan</label>
                            <div class="col-md-8">
                                <textarea class="form-control input-sm" placeholder="Permasalahan" name="masalah" id="masalah" value=""></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Penyuluhan</label>
                            <div class="col-md-8">
                                <textarea class="form-control input-sm" placeholder="Penyuluhan" name="rekomendasi" id="rekomendasi" value=""></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>