
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Bina extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        if ($_SESSION['role_id'] == '2') {
            // AGENT
            $data['js'] = 'js';
            $data['css'] = 'css';
            $data['content'] = 'bina_agent';
        } else if ($_SESSION['role_id'] == '3') {
            // TEAM LEADER
            $data['js'] = 'js';
            $data['css'] = 'css';
            $data['content'] = 'bina_tl';
        }
        $this->load->view('default', $data);
    }
}
