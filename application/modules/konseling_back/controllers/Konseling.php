
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Konseling extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'konseling';
        $data['kolom'] = array("Kode", "Agent", "Jenis Bina", "Masalah", "Tgl Kejadian", "Tgl Bina", "Tgl Verifikasi", "Status", "Opsi");
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vkonseling WHERE kode_tl=" . $_SESSION['kode'], 3);

        $this->load->view('default', $data);
    }

    public function listData()
    {
        if (IS_AJAX) {
            $aColumns = array("no", "kode", "agent", "jenis_bina_desc", "masalah", "tgl_kejadian", "tgl_bina", "tgl_verifikasi", "status", "kode");
            $sIndexColumn = "kode";
            $sTable = 'v' . $this->uri->segment(3);
            $sTablex = '';
            // $sWhere = ($_SESSION['kode_prodi'] <> '') ? " AND kode_prodi='".$_SESSION['kode_prodi']."' " : '';
            $sWhere = "";
            $sGroup = "";
            $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE 1=1 $sWhere $sGroup";

            echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
        }
    }

    function getAgent()
    {
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vkonseling WHERE kode=" . $this->input->post('kode'), 3);
        echo json_encode($q);
    }

    function simpanData()
    {
        $arrdata = array();
        $arrlayanan = array();
        $arrsegmen = array();
        $cid = '';

        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) { } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                switch (trim($key)):
                    case 'cid':
                        $cid = $value;
                        break;
                    default:
                        if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                            if (strlen(trim($value)) > 0) {
                                $tgl = explode("-", $value);
                                $newtgl = $tgl[1] . "-" . $tgl[0] . "-" . $tgl[2];
                                $time = strtotime($newtgl);
                                $arrdata[$key] = $value; // date('Y-m-d', $time);
                            } else {
                                $arrdata[$key] = NULL;
                            }
                        } else {
                            $arrdata[$key] = $value;
                        }
                        break;
                endswitch;
            }
        }

        if ($cid == "") {
            $datauser['username'] = $this->input->post('kode');
            $datauser['role'] = 2;
            $datauser['password'] = md5("garuda123");

            if (!$this->input->post('kode_layanan') == "") {
                $arrlayanan['kode_layanan']  = $this->input->post('kode_layanan');
                $arrlayanan['perner_id'] = $this->input->post('kode');
                $this->Data_model->simpanData($arrlayanan, 't_layanan');
            } else if ($this->input->post('kode_layanan') == "" && is_array($this->input->post('layanan'))) {
                for ($i = 0; $i < count($this->input->post('layanan')); $i++) {
                    $arrlayanan['kode_layanan'] = $this->input->post('layanan')[$i];
                    $arrlayanan['perner_id'] = $this->input->post('kode');
                    $this->Data_model->simpanData($arrlayanan, 't_layanan');;
                }
                // $arrlayanan['kode_layanan'] = join("#", $this->input->post('layanan'));
            } else {
                $arrlayanan['kode_layanan'] = $this->input->post('layanan')[0];
                $arrlayanan['perner_id'] = $this->input->post('kode');
                $this->Data_model->simpanData($arrlayanan, 't_layanan');
            }

            if (!$this->input->post('kode_segmen') == "") {
                $arrsegmen['kode_segmen']  = $this->input->post('kode_segmen');
                $arrsegmen['perner_id'] = $this->input->post('kode');
                $this->Data_model->simpanData($arrsegmen, 't_segmen');
            } else if ($this->input->post('kode_segmen') == "" && is_array($this->input->post('segmen'))) {
                // $arrsegmen['kode_segmen'] = join("#", $this->input->post('segmen'));
                for ($i = 0; $i < count($this->input->post('segmen')); $i++) {
                    $arrsegmen['kode_segmen'] = $this->input->post('segmen')[$i];
                    $arrsegmen['perner_id'] = $this->input->post('kode');
                    $this->Data_model->simpanData($arrsegmen, 't_segmen');
                }
            } else {
                $arrsegmen['kode_segmen'] = $this->input->post('segmen')[0];
                $arrsegmen['perner_id'] = $this->input->post('kode');
                $this->Data_model->simpanData($arrsegmen, 't_segmen');
            }

            $this->Data_model->simpanData($datauser, 'm_user');
            $this->Data_model->simpanData($arrdata, 't_' . $this->uri->segment(3));
        } else {
            $kondisi = 'kode';
            if (!$this->input->post('kode_segmen') == "") {
                $arrlayanan['kode_layanan'] = $this->input->post('layanan')[0];
                $arrlayanan['perner_id'] = $this->input->post('kode');
                $this->Data_model->simpanData($arrlayanan, 't_layanan');
            } else if ($this->input->post('kode_layanan') == "" && is_array($this->input->post('layanan'))) {
                $q = "DELETE FROM t_layanan WHERE perner_id IN (" . $this->input->post('kode') . ")";
                $this->Data_model->jalankanQuery($q);
                for ($i = 0; $i < count($this->input->post('layanan')); $i++) {
                    $arrsegmen['perner_id'] = $this->input->post('kode');
                    $arrsegmen['kode_layanan'] = $this->input->post('layanan')[$i];
                    $this->Data_model->simpanData($arrsegmen, 't_layanan');
                }
            }

            if ($this->input->post('kode_segmen') == "" && is_array($this->input->post('segmen'))) {
                $q = "DELETE FROM t_segmen WHERE perner_id IN (" . $this->input->post('kode') . ")";
                $this->Data_model->jalankanQuery($q);
                for ($i = 0; $i < count($this->input->post('segmen')); $i++) {
                    $arrsegmen['perner_id'] = $this->input->post('kode');
                    $arrsegmen['kode_segmen'] = $this->input->post('segmen')[$i];
                    $this->Data_model->simpanData($arrsegmen, 't_segmen');
                }
            }
            $this->Data_model->updateDataWhere($arrdata, 't_' . $this->uri->segment(3), array($kondisi => $cid));
            $this->Data_model->updateDataWhere($arrdata, 'm_user', array('username' => $cid));
        }
    }

    function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    $kondisi2 = array('username' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                    $kondisi2 = array('username' => $param);
                }
                $this->Data_model->hapusDataWhere('t_' . $this->input->post('cod'), $kondisi);
                $this->Data_model->hapusDataWhere('m_user', $kondisi2);
                echo json_encode("ok");
            }
        }
    }

    function view_detail()
    {
        $id = $this->uri->segment(3);
        $kondisi = "kode";
        $data['rowdata'] = $this->Data_model->satuData('vpegawai', array($kondisi => $id));
        $this->load->view('view_detail', $data);
    }

}
