<!-- Latest compiled and minified JavaScript -->
<script src="<?= base_url() ?>assets/admin/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/validator.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jszip.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/pdfmake.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/vfs_fonts.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/select2/select2.min.js"></script>

<div id="myConfirm" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan menghapus data <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($_SESSION['role_id'] == '2') : ?>
    <div id="updateActionPlan" class="modal animated--grow-in">
        <div class="modal-success">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Action Plan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="actionplanform" enctype="multipart/form-data" class="form-horizontal">
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Agent</label>
                                <div class="col-md-8">
                                    <select class="select2 form-control agent" name="kode_agent" id="agent" style="width:100%" disabled>
                                        <option value="">- Pilihan -</option>
                                        <?php
                                            $n = (isset($arey)) ? $arey['kode_agent'] : '';
                                            $q = $this->Data_model->selectData('m_users', 'kode', array('role_id' => 2));
                                            foreach ($q as $row) {
                                                $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                                echo '<option data-id="' . $row->kode . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->fullname . '</option>';
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Tanggal Kejadian</label>
                                <div class="col-md-8">
                                    <input type="date" class="form-control input-sm tgl_kejadian" name="tgl_kejadian" id="tgl_kejadian" value="" data-error="wajib diisi" required disabled>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Ketidaksesuaian</label>
                                <div class="col-md-8">
                                    <select class="select2 form-control kode_kts" name="kode_kts" id="kode_kts" style="width:100%" disabled>
                                        <option value="">- Pilihan -</option>
                                        <?php
                                            $n = (isset($arey)) ? $arey['kode_kts'] : '';
                                            $q = $this->Data_model->selectData('m_kts_subkategori', 'kode', array('jenis_bina' => '1'));
                                            foreach ($q as $row) {
                                                $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                                echo '<option data-num="' . $row->kts_verifikasi_num . '" data-str="' . $row->kts_verifikasi_str . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->kts_subkategori . '</option>';
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Waktu Verifikasi</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control input-sm waktu_verifikasi" placeholder="Waktu Verifikasi" name="waktu_verifikasi" disabled>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Tanggal Verifikasi</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control input-sm tgl_verifikasi" placeholder="Tanggal Verifikasi" name="tgl_verifikasi" disabled>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Permasalahan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control input-sm masalah" placeholder="Permasalahan" name="masalah" id="masalah" value="" disabled></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Penyuluhan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control input-sm rekomendasi" placeholder="Penyuluhan" name="rekomendasi" id="rekomendasi" value="" disabled></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 label-control">Action Plan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control input-sm" placeholder="Action Plan" name="Action Plan" id="action_plan" value=""></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <script>
        $(function() {
            $("#AntrianTable").dataTable();
            $('#openform').on('click', function() {
                $('#modalForm').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
            $('#modalForm').on('hidden.bs.modal', function() {
                $(this).find('form')[0].reset();
                if ($("#pesanan tbody tr").length > 1) {
                    $("#pesanan tbody tr:last-child").remove();
                }
            });
            $(".edit").on('click', function() {
                $("#updateActionPlan").modal();
                $.ajax({
                    url: "<?= base_url('coaching/getAgent') ?>",
                    type: "POST",
                    data: "kode=" + $(this).data('kode'),
                    dataType: "json",
                    success: function(html) {
                        $(".agent").find("option[value=" + html[0].kode_agent + "]").prop('selected', true);
                        $(".tgl_kejadian").val(html[0].tgl_kejadian);
                        $(".kode_kts").find("option[value=" + html[0].kode_kts + "]").prop('selected', true);
                        $(".waktu_verifikasi").val(html[0].kts_verifikasi_str);
                        $(".tgl_verifikasi").val(html[0].tgl_verifikasi);
                        $(".masalah").text(html[0].masalah);
                        $(".rekomendasi").text(html[0].rekomendasi);
                    }
                })
            })
            $(".delete").on('click', function() {
                $("#myConfirm").modal();
                $(".lblModal").text($(this).data('kode'));
                $("#cid").val($(this).data('kode'));
                $("#getto").val("<?= base_url('antrian/hapus') ?>");
                $("#cod").val("t_antrian");
            })
            $(".detail").on('click', function() {
                $('#tabelpegawai').hide();
                $('#contentdetail').load('' + 'antrian/view_detail/' + $(this).attr("data-kode"));
                $('#containerdetail').fadeIn('fast');
            });
            $('.kecamatan').select2();
            $("#btnYes").bind("click", function() {
                var link = $("#getto").val();
                $.ajax({
                    url: link,
                    type: "POST",
                    data: "cid=" + $("#cid").val() + "&cod=" + $("#tabel").val(),
                    dataType: "html",
                    beforeSend: function() {
                        if (link != "#") {}
                    },
                    success: function(html) {
                        notify("Delete berhasil", "danger")
                        $("#myConfirm").modal("hide")
                        location.reload(true);
                    }
                })
            });

        });
    </script>
    <script>
        $(function() {
            var a = $("#pesanan tbody tr").length;

            $(".addpesanan").on("click", function() {
                a++;
                var x = '<td style="text-align: center;">' + (a) + "</td>";
                x += '<td><select class="select2 pilihjenis form-control" id="selectjenis' + (a) + '" name="jenis[]" onchange="loadBarang(' + a + ')"></select></td>';
                x += '<td id="barang' + (a) + '"></td>';
                x += '<td id="jumlah' + (a) + '"></td>';
                $("#pesanan tbody").append('<tr id="row' + (a) + '">' + x + "</tr>");
                selectPesanan(a);
            });

            $(".removepesanan").on("click", function() {
                if ($("#pesanan tbody tr").length > 1) {
                    $("#pesanan tbody tr:last-child").remove();
                    a--;
                } else {
                    alert("Baris pertama isian tidak dapat dihapus")
                }
            });
        });
        bukaListJenis();

        function bukaListJenis() {
            if ($("#cid").val() == "") {
                selectPesanan(1);
            } else {
                $.each($(".pilijenis"), function(j, g) {
                    var c = "selectjenis" + (j + 1);
                    var b = $("#selectjenis" + (j + 1)).attr("data-default");
                    getListCIS("m_jenis", c, "1", "1", $("#selectjenis" + (j + 1)).attr("data-default"), "kode,DISTINCT(tipe) as tipe", "Pilihan", "antrian/getList/");
                });

            }
        }

        function selectPesanan(urutan) {
            $.ajax({
                url: "antrian/select_jenis",
                type: "POST",
                success: function(html) {
                    json = eval(html);
                    $("#selectjenis" + urutan + "").append('<option value="">Pilihan</option>');
                    $(json).each(function() {
                        $("#selectjenis" + urutan + " ").append('<option value="' + this.disp + '">' + this.disp + "</option>");
                    });
                }
            })
        }

        function cekMax(urutan) {
            var dat = [];
            $("#selectjumlah" + urutan).on('change', function() {
                var selected = $(this).children("option:selected").val();
                if (selected !== "") {
                    dat.push(selected);
                } else {

                }
            })
            console.log(dat);
        }

        function loadBarang(urutan) {
            var tipe = $("#selectjenis" + urutan).val();
            var opt = "";
            if (tipe == "BENSIN") {
                opt += "<option value=''>- Pilihan -</option>";
                opt += "<option value=1>10 Liter</option>";
                opt += "<option value=2>20 Liter</option>";
                opt += "<option value=3>30 Liter</option>";
                $("#selectjumlah" + urutan + " ").remove();
                $("#selectbarang" + urutan + " ").remove();
                $("#barang" + urutan).append('<select class="select2 pilihbarang' + urutan + ' form-control" id="selectbarang' + urutan + '" name="barang[]"></select>');
                $("#jumlah" + urutan).append('<select class="select2 pilihjumlah form-control" id="selectjumlah' + urutan + '" name="jumlah[]"></select>');
                $("#selectjumlah" + urutan + " ").append(opt);
                // $("#selectjumlah" + urutan).bind('change', 'option', function(){
                //     if ($(this).val() == ""){
                //         console.log("kosong");
                //     }else{
                //         var itung = $(this).val() * 10;
                //         if (itung >= 30){
                //             console.log(itung);
                //         }else{
                //             console.log(itung);
                //         }
                //     }
                // })
            } else {
                $("#selectjumlah" + urutan + " ").remove();
                $("#selectbarang" + urutan + " ").remove();
                $("#barang" + urutan).append('<select class="select2 pilihbarang form-control" id="selectbarang' + urutan + '" name="barang[]"></select>');
                $("#jumlah" + urutan).append('<input type="number" class="form-control txtjumlah" id= "selectjumlah' + urutan + '" name="jumlah[]" placeholder="Jumlah">')
            }

            $.ajax({
                type: 'GET',
                url: "<?= base_url('antrian/getBarang'); ?>",
                data: "tipe=" + tipe,
                success: function(html) {
                    json = eval(html);
                    $("#selectbarang" + urutan + "").append('<option value="">Pilihan</option>');
                    $(json).each(function() {
                        $("#selectbarang" + urutan + " ").append('<option value="' + this.kode + '">' + this.disp + "</option>")
                    });
                }
            })

        }

        function loadSPBU() {
            var kec = $("#kecamatan").val();
            $.ajax({
                type: 'GET',
                url: "<?= base_url('antrian/getSPBU'); ?>",
                data: "kode=" + kec,
                success: function(html) {
                    $("#spbuArea").html(html);
                    $('.spbu').select2();
                }
            });
        }
    </script>