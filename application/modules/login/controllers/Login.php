<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Login extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (isset($_SESSION['email'])) {
            redirect('../dashboard');
        }
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'login';

        $this->load->view('layout_login', $data);
    }

    public function doLogin()
    {
        if (IS_AJAX) {
            $user = $this->db->get_where('m_users', ['email' => $this->input->post('email')])->row_array();
            if ($user) {
                $res = $this->Data_model->verify_user(array('email' => $this->input->post('email'), 'password' => password_verify($this->input->post('password1'), $user['password'])), 'm_users');
                if ($res->is_active == 1) {
                    if ($res !== FALSE) {
                        foreach ($res as $row => $kolom) {
                            $_SESSION[$row] = $kolom;
                        }
                        echo base_url('dashboard');
                    } else {
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button>Wrong password!';
                    }
                } else {
                    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button>This email has not been activated!';
                }
            } else {
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button>This email is not registered!';
            }
        }
    }

    public function doOut()
    {
        session_unset();
        session_destroy();
        redirect('login');
    }
}
