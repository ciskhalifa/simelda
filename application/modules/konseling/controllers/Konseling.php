
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Konseling extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'konseling';
        $data['kolom'] = array("Kode", "Agent", "Jenis Bina", "Masalah", "Tgl Kejadian", "Tgl Bina", "Tgl Verifikasi", "Status", "Opsi");
        if ($_SESSION['role_id'] == 3) {
            $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vkonseling WHERE kode_tl=" . $_SESSION['kode'], 3);
        } else {
            $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vkonseling WHERE kode_agent=" . $_SESSION['kode'], 3);
        }
        $this->load->view('default', $data);
    }

    public function listData()
    {
        if (IS_AJAX) {
            $aColumns = array("no", "kode", "agent", "jenis_bina_desc", "masalah", "tgl_kejadian", "tgl_bina", "tgl_verifikasi", "status", "kode");
            $sIndexColumn = "kode";
            $sTable = 'v' . $this->uri->segment(3);
            $sTablex = '';
            // $sWhere = ($_SESSION['kode_prodi'] <> '') ? " AND kode_prodi='".$_SESSION['kode_prodi']."' " : '';
            $sWhere = "";
            $sGroup = "";
            $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE 1=1 $sWhere $sGroup";

            echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
        }
    }
    public function updateBina()
    {
        if (IS_AJAX) {
            $arrdata['status'] = $this->input->post('status');
            $this->Data_model->updateDataWhere($arrdata, 't_konseling', array('kode' => $this->input->post('kode')));
            echo json_encode("ok");
        }
    }
    public function updateActionPlan()
    {
        if (IS_AJAX) {
            $arrdata['action_plan'] = $this->input->post('action_plan');
            $this->Data_model->updateDataWhere($arrdata, 't_konseling', array('kode' => $this->input->post('kode')));
            echo json_encode("ok");
        }
    }
    public function simpanData()
    {

        $arrdata = array();
        $cid = '';

        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) {} else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                switch (trim($key)):
            case 'cid':
                $cid = $value;
                break;
            default:
                if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                        if (strlen(trim($value)) > 0) {
                            $tgl = explode("-", $value);
                            $newtgl = $tgl[1] . "-" . $tgl[0] . "-" . $tgl[2];
                            $time = strtotime($newtgl);
                            $arrdata[$key] = $value; // date('Y-m-d', $time);
                        } else {
                            $arrdata[$key] = null;
                        }
                } else {
                    $arrdata[$key] = $value;
                }
                break;
                endswitch;
            }
        }

        if ($cid == "") {
            $arrdata['kode_tl'] = $_SESSION['kode'];
            $this->Data_model->simpanData($arrdata, 't_' . $this->uri->segment(1));
        } else {
            $kondisi = 'kode';
            $this->Data_model->updateDataWhere($arrdata, 't_' . $this->uri->segment(1), array($kondisi => $cid));
        }
        redirect('konseling');
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    $kondisi2 = array('username' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                    $kondisi2 = array('username' => $param);
                }
                $this->Data_model->hapusDataWhere('t_' . $this->input->post('cod'), $kondisi);
                echo json_encode("ok");
            }
        }
    }
    public function getAgent()
    {
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vkonseling WHERE kode=" . $this->input->post('kode'), 3);
        echo json_encode($q);
    }

    public function view_detail()
    {
        $id = $this->uri->segment(3);
        $kondisi = "kode";
        $data['rowdata'] = $this->Data_model->satuData('vpegawai', array($kondisi => $id));
        $this->load->view('view_detail', $data);
    }

}
