
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Dashboard extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        if ($_SESSION['role_id'] == '1') {
            // SUPERADMIN
            $data['js'] = 'js';
            $data['css'] = 'css';
            $data['content'] = 'dashboard';
        } else if ($_SESSION['role_id'] == '2') {
            // AGENT
            $data['js'] = 'js';
            $data['css'] = 'css';
            $data['content'] = 'dashboard_agent';
            $data['trans'] = $this->Data_model->getTotalData('t_bina', array('kode_agent' => $_SESSION['kode'], ''));
        } else if ($_SESSION['role_id'] == '3') {
            // TEAM LEADER
            $data['js'] = 'js';
            $data['css'] = 'css';
            $data['content'] = 'dashboard_tl';
        }
        $this->load->view('default', $data);
    }
}
