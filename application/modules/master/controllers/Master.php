<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Master extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        // halaman
        $data['content'] = 'master';
        // js tambahan dan css
        $data['js'] = 'js';
        $data['css'] = 'css';

        $this->load->view('default', $data);
    }

    public function loadHalaman()
    {
        $konten['tabel'] = $this->uri->segment(3); // barang
        switch ($this->uri->segment(3)):
            case 'jenis_bina':
                $konten['kolom'] = array("Kode", "Jenis Bina", "Opsi");
                break;
            case 'kts_kategori':
                $konten['kolom'] = array("Kode", "Kategori", "Opsi");
                break;
            case 'kts_subkategori':
                $konten['kolom'] = array("Kode", "Bina", "Kategori", "SubKategori", "Form", "Verifikasi", "Opsi");
                break;
            case 'users':
                $konten['kolom'] = array("Kode", "Nama Lengkap", "Email", "Role", "Opsi");
                break;
            default:
                $konten['kolom'] = "";
                break;
        endswitch;

        $konten['jmlkolom'] = count($konten['kolom']);
        $this->load->view('halaman', $konten);
    }

    public function getData()
    {
        /*
             * list data
             */
        if (IS_AJAX) {
            $sTablex = "";
            $order = "";
            $sTable = 'm_' . $this->uri->segment(3); // m_barang
            $k = '';
            switch ($this->uri->segment(3)):
                case 'jenis_bina':
                    $aColumns = array("kode", "jenis_bina_desc", "opsi");
                    $kolom = "kode,jenis_bina_desc";
                    $where = " 1=1";
                    $sIndexColumn = "kode";
                    break;
                case 'kts_kategori':
                    $aColumns = array("kode", "kts_kategori_desc", "opsi");
                    $kolom = "kode,kts_kategori_desc";
                    $where = " 1=1";
                    $sIndexColumn = "kode";
                    break;
                case 'kts_subkategori':
                    $aColumns = array("kode", "jenis_bina_desc", "kts_kategori_desc", "kts_subkategori", "jenis_form", "kts_verifikasi_str", "opsi");
                    $kolom = "kode,kts_subkategori, jenis_form, kts_verifikasi_str, b.jenis_bina_desc, c.kts_kategori_desc";
                    $where = " 1=1";
                    $sIndexColumn = "kode";
                    break;
                case 'users':
                    $aColumns = array("kode", "fullname", "email", "role_id", "is_active", "opsi");
                    $kolom = "kode, fullname, email,CASE WHEN role_id = '1' THEN 'Super Admin' WHEN '2' THEN 'Agent' ELSE '??' END  AS role_id, CASE WHEN is_active = '1' THEN 'Actived' ELSE 'Not Actived' END as is_active";
                    $where = "1=1";
                    $sIndexColumn = "kode";
                    break;
                default:

                    break;
            endswitch;
            if (isset($kolom) && strlen($kolom) > 0) {

                if ($this->uri->segment(3) == "kts_subkategori") {
                    $tQuery = "SELECT $kolom, '$k' AS opsi "
                        . "FROM $sTable JOIN m_jenis_bina as b ON b.kode = jenis_bina JOIN m_kts_kategori as c ON c.kode = kts_kategori WHERE $where";
                    echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
                } else {
                    $tQuery = "SELECT $kolom ,'$k' AS opsi  "
                        . "FROM $sTable a $sTablex WHERE $where ";
                    echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
                }
            } else {
                echo "";
            }
        }
    }

    public function loadForm()
    {
        $konten['aep'] = $this->uri->segment(5);
        if ($this->uri->segment(4) != '-') {
            $nilai = $this->uri->segment(4);
            if (strtoupper($this->uri->segment(5)) == 'SALIN') {
                if ($this->uri->segment(6) == '' || strlen(trim($this->uri->segment(6))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(6)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            } else {
                if ($this->uri->segment(5) == '' || strlen(trim($this->uri->segment(5))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(5)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            }
            $konten['rowdata'] = $this->Data_model->satuData($tabel, $kondisi);
            if (strtoupper($this->uri->segment(5)) == 'SALIN') {
                $konten['jenis'] = ($this->uri->segment(6) == '') ? 'A' : strtoupper($this->uri->segment(6));
            } else {
                $konten['jenis'] = strtoupper($this->uri->segment(5));
            }
        } else {
            $konten['jenis'] = strtoupper($this->uri->segment(5));
        }
        $this->load->view('form/' . $this->uri->segment(3), $konten);
    }

    public function simpanData()
    {
        $arrdata = array();
        $cid = '';
        $tabel = 'm_' . $this->uri->segment(3);
        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) { } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                if ($key == 'cid') {
                    $cid = $value;
                } else {
                    if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                        if (strlen(trim($value)) > 0) {
                            $tgl = explode("/", $value);
                            $newtgl = $tgl[1] . "/" . $tgl[0] . "/" . $tgl[2];
                            $time = strtotime($newtgl);
                            $arrdata[$key] = date('Y-m-d', $time);
                        } else {
                            $arrdata[$key] = null;
                        }
                    } else {
                        if ($this->uri->segment(3) == "users") {
                            $arrdata['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                            $arrdata[$key] = $value;
                        }
                        $arrdata[$key] = $value;
                    }
                }
            }
        }
        if ($cid == "") {
            try {
                $this->Data_model->simpanData($arrdata, $tabel);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        } else {
            $kondisi = 'kode';
            try {
                $kondisi = array('kode' => $cid);
                echo $this->Data_model->updateDataWhere($arrdata, $tabel, $kondisi);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        }
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->hapusDataWhere('m_' . $this->input->post('cod'), $kondisi);
                echo json_encode("ok");
            }
        }
    }

    function getKabupaten()
    {
        $kabupaten = $this->Data_model->ambilDataWhere('m_kota', array('kode_provinsi' => $_GET['kode']), 'nama', 'asc');
        echo '<select class="select2 form-control kota" name="kota" id="kota" onchange="loadKecamatan()">' .
            '<option value="">- Pilihan -</option>';
        foreach ($kabupaten as $row) {
            echo '<option data-id="' . $row->nama . '" value="' . $row->kode . '">' . $row->nama . '</option>';
        }
        echo '</select>';
    }

    function getKecamatan()
    {
        $kabupaten = $this->Data_model->ambilDataWhere('m_kecamatan', array('kode_kota' => $_GET['kode']), 'nama', 'asc');
        echo '<select class="select2 form-control kecamatan" name="kecamatan" id="kecamatan">' .
            '<option value="">- Pilihan -</option>';
        foreach ($kabupaten as $row) {
            echo '<option data-id="' . $row->nama . '" value="' . $row->kode . '">' . $row->nama . '</option>';
        }
        echo '</select>';
    }
}
