<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $cid = ($aep == 'salin') ? '' : $rowdata->kode;
    $kts_subkategori = $rowdata->kts_subkategori;
    $kts_desc = $rowdata->kts_desc;
    $jenis_form = $rowdata->jenis_form;
    $kts_verifikasi_str = $rowdata->kts_verifikasi_str;
    $kts_verifikasi_num = $rowdata->kts_verifikasi_num;
} else {
    $cid = "";
    $kts_subkategori = "";    
    $kts_desc = "";    
    $jenis_form = "";    
    $kts_verifikasi_str = "";    
    $kts_verifikasi_num = "";    
}
?>
<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal" method="POST">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group row">
            <label class="col-md-2 label-control">Pembinaan</label>
            <div class="col-md-6">
                <select class="select2 form-control jenis_bina" name="jenis_bina" id="jenis_bina">
                    <option value="">- Pilihan -</option>
                    <?php
                    $n = (isset($arey)) ? $arey['jenis_bina'] : '';
                    $q = $this->Data_model->selectData('m_jenis_bina', 'kode');
                    foreach ($q as $row) {
                        $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                        echo '<option data-id="' . $row->jenis_bina_desc . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->jenis_bina_desc . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Kategori</label>
            <div class="col-md-6">
                <select class="select2 form-control kts_kategori" name="kts_kategori" id="kts_kategori">
                    <option value="">- Pilihan -</option>
                    <?php
                    $n = (isset($arey)) ? $arey['kts_kategori'] : '';
                    $q = $this->Data_model->selectData('m_kts_kategori', 'kode');
                    foreach ($q as $row) {
                        $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                        echo '<option data-id="' . $row->kts_kategori_desc . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->kts_kategori_desc . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">SubKategori</label>
            <div class="col-md-6">
                <input type="text" class="form-control input-sm" placeholder="Sub Kategori" name="kts_subkategori" id="kts_subkategori" value="<?php echo $kts_subkategori; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Deskripsi</label>
            <div class="col-md-6">
                <textarea class="form-control" placeholder="Deskripsi" name="kts_desc" id="kts_desc"><?= $kts_desc; ?></textarea>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Jenis Form</label>
            <div class="col-md-6">
                <input type="text" class="form-control input-sm" placeholder="Jenis Form" name="jenis_form" id="jenis_form" value="<?php echo $jenis_form; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">KTS Verifikasi Str</label>
            <div class="col-md-6">
                <input type="text" class="form-control input-sm" placeholder="KTS Verifikasi Str" name="kts_verifikasi_str" id="kts_verifikasi_str" value="<?php echo $kts_verifikasi_str; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">KTS Verifikasi Num</label>
            <div class="col-md-6">
                <input type="number" class="form-control input-sm" placeholder="KTS Verifikasi Num" name="kts_verifikasi_num" id="kts_verifikasi_num" value="<?php echo $kts_verifikasi_num; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-actions">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script>
    $(function() {
        $("#tmblBatal").on("click", function() {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function(c) {
            if (c.isDefaultPrevented()) {} else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = new FormData($('#xfrm')[0]);
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    cache: false,
                    contentType: false,
                    processData: false,
                    //dataType: "html",
                    beforeSend: function() {
                        $(".card #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: ''
                        })
                    },
                    success: function(d) {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function() {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>