/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.1.38-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `t_coaching` (
	`kode` int (11),
	`kode_agent` int (11),
	`kode_kts` int (11),
	`jenis_bina` int (11),
	`tgl_kejadian` date ,
	`tgl_bina` date ,
	`kode_dept` int (11),
	`kode_site` varchar (9),
	`masalah` text ,
	`rekomendasi` text ,
	`tgl_verifikasi` date ,
	`kode_tl` int (11),
	`action_plan` text ,
	`hasil_verifikasi` text ,
	`approve_agent` datetime ,
	`approve_atasan` datetime ,
	`approve_pembina` datetime ,
	`status` varchar (60)
); 
insert into `t_coaching` (`kode`, `kode_agent`, `kode_kts`, `jenis_bina`, `tgl_kejadian`, `tgl_bina`, `kode_dept`, `kode_site`, `masalah`, `rekomendasi`, `tgl_verifikasi`, `kode_tl`, `action_plan`, `hasil_verifikasi`, `approve_agent`, `approve_atasan`, `approve_pembina`, `status`) values('1','2','3','1','2019-09-19','2019-09-20','1','BDO','test','test	','2019-09-27','3',NULL,NULL,NULL,NULL,NULL,NULL);
