/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.1.38-MariaDB : Database - db_simelda
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_simelda` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_simelda`;

/*Table structure for table `m_jenis_bina` */

DROP TABLE IF EXISTS `m_jenis_bina`;

CREATE TABLE `m_jenis_bina` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_bina_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `m_jenis_bina` */

insert  into `m_jenis_bina`(`kode`,`jenis_bina_desc`) values 
(1,'Teguran'),
(2,'BATL'),
(3,'Surat Peringatan'),
(4,'Pengembalian ke PPJP');

/*Table structure for table `m_kts_kategori` */

DROP TABLE IF EXISTS `m_kts_kategori`;

CREATE TABLE `m_kts_kategori` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kts_kategori_desc` text,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `m_kts_kategori` */

insert  into `m_kts_kategori`(`kode`,`kts_kategori_desc`) values 
(1,'Ketidaksesuaian Implementasi Tata Tertib Umum'),
(2,'Ketidaksesuaian Implementasi Ketentuan Umum'),
(3,'Ketidaksesuaian Jam Kerja'),
(4,'Ketidaksesuaian Pelayanan terhadap Penumpang'),
(5,'Ketidaksesuaian Target Kerja'),
(6,'Ketidaksesuaian Implementasi Tata Tertib Khusus (Attitude)'),
(7,'Kesalahan Informasi Kepada Pelanggan');

/*Table structure for table `m_kts_subkategori` */

DROP TABLE IF EXISTS `m_kts_subkategori`;

CREATE TABLE `m_kts_subkategori` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kts_subkategori` text,
  `kts_desc` text,
  `kts_kategori` int(11) DEFAULT NULL,
  `jenis_bina` int(11) DEFAULT NULL,
  `jenis_form` varchar(10) DEFAULT NULL,
  `kts_verifikasi_str` varchar(255) DEFAULT NULL,
  `kts_verifikasi_num` int(11) DEFAULT NULL,
  `state` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `m_kts_subkategori` */

insert  into `m_kts_subkategori`(`kode`,`kts_subkategori`,`kts_desc`,`kts_kategori`,`jenis_bina`,`jenis_form`,`kts_verifikasi_str`,`kts_verifikasi_num`,`state`) values 
(1,'Ketidaksesuaian Implementasi Tata Tertib Umum',NULL,1,1,'coaching','1 Week',7,'1'),
(2,'Ketidaksesuaian Implementasi Ketentuan Umum',NULL,2,1,'coaching','1 Week',7,'1'),
(3,'Keterlambatan &lt; 10 Menit',NULL,3,1,'coaching','1 Week',7,'1'),
(4,'Keterlambatan &gt; 10 Menit',NULL,3,1,'coaching','1 Month',30,'1'),
(5,'Kelebihan Aux (Tanpa Adjusment)',NULL,3,1,'coaching','1 Week',7,'1'),
(6,'Ketidaksesuaian Aux (Based On Desk Control Report)',NULL,3,1,'coaching','1 Week',7,'1'),
(7,'Kekurangan Jam Kerja (Tanpa Adjustment)',NULL,3,1,'coaching','1 Week',7,'1'),
(8,'Lupa Logout yang tidak mengakibatkan Lost Call',NULL,3,1,'coaching','1 Month',30,'1'),
(9,'Lupa Logout dalam keadaan Auto In dan mengakibatkan Lost Call',NULL,3,1,'konseling','1 Month',30,'1'),
(10,'Penyalahgunaan Proses Holding ',NULL,4,1,'coaching','1 Week',7,'1'),
(11,'Tidak mengakhiri percakapan\r\n','membiarkan panggilan yang sudah selesai dilayani masih dalam keadaan on call',4,1,'coaching','1 Week',7,'1'),
(12,'Ketidaksesuaian Informasi dan Penanganan Customer (NC Solusi dari QCO)',NULL,4,1,'coaching','1 Week',7,'1'),
(13,'Update Staff Time/Aux',NULL,4,1,'coaching','1 Week',7,'1'),
(14,'Call Release (Per Kejadian/History) (Tanpa Adjusment)',NULL,4,1,'konseling',NULL,NULL,'1'),
(15,'Melakukan Intimidasi kepada pelanggan, berkata kasar dan tidak sopan, memberikan pelayanan tidak sesuai dengan standar',NULL,4,2,'batl',NULL,NULL,'1'),
(16,'Average QM Score per bulan &lt; 90',NULL,5,1,'coaching','1 Month',30,'1'),
(17,'Ketidakhadiran &lt; 100% dalam 3 Bulan berturut-turut',NULL,5,1,'batl',NULL,NULL,'1'),
(18,'PNP Score &lt; 90',NULL,5,1,'coaching',NULL,NULL,'1'),
(19,'Tidak mencapai target pendapatan 3 Bulan berturut-turut (Tanpa Adjusment)','Khusus Telesales Travel Assistant Outbond',5,1,'coaching',NULL,NULL,'1'),
(20,'Tidak Hadir Tanpa Keterangan',NULL,6,2,'batl',NULL,NULL,'1'),
(21,'Tidak menjalankan kebijakan yang sudah ditetapkan untuk mencapai sasaran mutu/target perusahaan',NULL,6,2,'batl',NULL,NULL,'1'),
(22,'Merokok di area ruangan call center/toilet',NULL,6,2,'batl',NULL,NULL,'1'),
(23,'Menggunakan Perangkat tidak untuk kepentingan bekerja','Main Games, Film, Transfer Data etc',6,2,'batl',NULL,NULL,'1'),
(24,'Melakukan tindakan yang tidak sesuai norma','perbuatan asusila di tempat kerja, mabuk, madat, memakai obat bius atau obat terlarang',6,4,'ppjp',NULL,NULL,'1'),
(25,'Melakukan tindakan kejahatan','mencuri, menggelapkan,memperdagangkan barang-barang terlarang, berjudi didalam lingkungan perusahaan maupun diluar perusahaan, etc',6,4,'ppjp',NULL,NULL,'1'),
(26,'Melakukan penganiayaan, menghina secara kasar, mengancam atasan atau rekan kerja GCC Garuda Indonesia',NULL,6,4,'ppjp',NULL,NULL,'1'),
(27,'Memberikan informasi rahasia perusahaan kepada pelanggan dan pemalsuan dokumen',NULL,6,4,'ppjp',NULL,NULL,'1'),
(28,'Mencemarkan nama baik perusahaan','Garuda Indonesia, Infomedia dan ppjp terkait',6,4,'ppjp',NULL,NULL,'1'),
(29,'Kesalahan Informasi kepada pelanggan yang bersifat Minor','Tidak mengakibatkan TuntutanHukum, pelaporkan ke Media Massa, Komplain BOD, hingga GA harus mengeluarkan kompensasi',7,1,'coaching','1 Month',30,'1'),
(30,'Kesalahan Informasi kepada pelanggan yang bersifat Major','Mengakibatkan Tuntutan Hukum, Pelaporkan ke Media Massa, Komplain BOD, hingga GA harus mengeluarkan Kompensasi',7,3,'sp','6 Month',180,'1');

/*Table structure for table `m_users` */

DROP TABLE IF EXISTS `m_users`;

CREATE TABLE `m_users` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `m_users` */

insert  into `m_users`(`kode`,`fullname`,`email`,`image`,`password`,`role_id`,`is_active`,`date_created`) values 
(1,'Administrator','admin@garudazone.com','default.jpg','$2y$10$Ra.2G0YdMTPTgIm2cUz7xONdPF6G3i.Yj14NbxHw5GoFV2QopDQlu',1,'1','2019-09-17 15:54:43'),
(2,'Agent','agent@garudazone.com',NULL,'$2y$10$e7rRRADdQ75mggCFgoac4.8ezB7XcWNCT6Uzt35p6f5NYu5krYvfG',2,'1','2019-09-18 15:22:06'),
(3,'Team Leader','tl@garudazone.com','default.jpg','$2y$10$e7rRRADdQ75mggCFgoac4.8ezB7XcWNCT6Uzt35p6f5NYu5krYvfG',3,'1','2019-09-19 10:21:21');

/*Table structure for table `t_batl` */

DROP TABLE IF EXISTS `t_batl`;

CREATE TABLE `t_batl` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_agent` int(11) DEFAULT NULL,
  `kode_kts` int(11) DEFAULT NULL,
  `jenis_bina` int(11) DEFAULT NULL,
  `tgl_kejadian` date DEFAULT NULL,
  `tgl_bina` date DEFAULT NULL,
  `kode_dept` int(11) DEFAULT NULL,
  `kode_site` varchar(9) DEFAULT NULL,
  `masalah` text,
  `rekomendasi` text,
  `tgl_verifikasi` date DEFAULT NULL,
  `kode_tl` int(11) DEFAULT NULL,
  `action_plan` text,
  `hasil_verifikasi` text,
  `approve_agent` datetime DEFAULT NULL,
  `approve_atasan` datetime DEFAULT NULL,
  `approve_pembina` datetime DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_batl` */

/*Table structure for table `t_bina` */

DROP TABLE IF EXISTS `t_bina`;

CREATE TABLE `t_bina` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_agent` int(11) DEFAULT NULL,
  `kode_kts` int(11) DEFAULT NULL,
  `tgl_kejadian` date DEFAULT NULL,
  `tgl_bina` date DEFAULT NULL,
  `jenis_bina` int(11) DEFAULT NULL,
  `kode_dept` int(11) DEFAULT NULL,
  `kode_site` varchar(3) DEFAULT NULL,
  `masalah` text,
  `rekomendasi` text,
  `tgl_verifikasi` date DEFAULT NULL,
  `kode_tl` int(11) DEFAULT NULL,
  `action_plan` text,
  `hasil_verifikasi` text,
  `approve_agent` datetime DEFAULT NULL,
  `approve_atasan` datetime DEFAULT NULL,
  `approve_pembina` datetime DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_bina` */

/*Table structure for table `t_coaching` */

DROP TABLE IF EXISTS `t_coaching`;

CREATE TABLE `t_coaching` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_agent` int(11) DEFAULT NULL,
  `kode_kts` int(11) DEFAULT NULL,
  `jenis_bina` int(11) DEFAULT NULL,
  `tgl_kejadian` date DEFAULT NULL,
  `tgl_bina` date DEFAULT NULL,
  `kode_dept` int(11) DEFAULT NULL,
  `kode_site` varchar(3) DEFAULT NULL,
  `masalah` text,
  `rekomendasi` text,
  `tgl_verifikasi` date DEFAULT NULL,
  `kode_tl` int(11) DEFAULT NULL,
  `action_plan` text,
  `hasil_verifikasi` text,
  `approve_agent` datetime DEFAULT NULL,
  `approve_atasan` datetime DEFAULT NULL,
  `approve_pembina` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_coaching` */

/*Table structure for table `t_konseling` */

DROP TABLE IF EXISTS `t_konseling`;

CREATE TABLE `t_konseling` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_agent` int(11) DEFAULT NULL,
  `kode_kts` int(11) DEFAULT NULL,
  `jenis_bina` int(11) DEFAULT NULL,
  `tgl_kejadian` date DEFAULT NULL,
  `tgl_bina` date DEFAULT NULL,
  `kode_dept` int(11) DEFAULT NULL,
  `kode_site` varchar(9) DEFAULT NULL,
  `masalah` text,
  `rekomendasi` text,
  `tgl_verifikasi` date DEFAULT NULL,
  `kode_tl` int(11) DEFAULT NULL,
  `action_plan` text,
  `hasil_verifikasi` text,
  `approve_agent` datetime DEFAULT NULL,
  `approve_atasan` datetime DEFAULT NULL,
  `approve_pembina` datetime DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_konseling` */

/*Table structure for table `t_sp` */

DROP TABLE IF EXISTS `t_sp`;

CREATE TABLE `t_sp` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_agent` int(11) DEFAULT NULL,
  `kode_kts` int(11) DEFAULT NULL,
  `jenis_bina` int(11) DEFAULT NULL,
  `tgl_kejadian` date DEFAULT NULL,
  `tgl_bina` date DEFAULT NULL,
  `kode_dept` int(11) DEFAULT NULL,
  `kode_site` varchar(9) DEFAULT NULL,
  `masalah` text,
  `rekomendasi` text,
  `tgl_verifikasi` date DEFAULT NULL,
  `kode_tl` int(11) DEFAULT NULL,
  `action_plan` text,
  `hasil_verifikasi` text,
  `approve_agent` datetime DEFAULT NULL,
  `approve_atasan` datetime DEFAULT NULL,
  `approve_pembina` datetime DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_sp` */

/*Table structure for table `vbatl` */

DROP TABLE IF EXISTS `vbatl`;

/*!50001 DROP VIEW IF EXISTS `vbatl` */;
/*!50001 DROP TABLE IF EXISTS `vbatl` */;

/*!50001 CREATE TABLE  `vbatl`(
 `kode` int(11) ,
 `kode_agent` int(11) ,
 `kode_kts` int(11) ,
 `jenis_bina` int(11) ,
 `tgl_kejadian` date ,
 `tgl_bina` date ,
 `kode_dept` int(11) ,
 `kode_site` varchar(9) ,
 `masalah` text ,
 `rekomendasi` text ,
 `tgl_verifikasi` date ,
 `kode_tl` int(11) ,
 `action_plan` text ,
 `hasil_verifikasi` text ,
 `approve_agent` datetime ,
 `approve_atasan` datetime ,
 `approve_pembina` datetime ,
 `status` varchar(60) ,
 `agent` varchar(100) ,
 `tl` varchar(100) ,
 `jenis_form` varchar(10) ,
 `kts_verifikasi_str` varchar(255) ,
 `kts_subkategori` text ,
 `kts_desc` text ,
 `jenis_bina_desc` varchar(255) ,
 `kts_kategori_desc` text 
)*/;

/*Table structure for table `vcoaching` */

DROP TABLE IF EXISTS `vcoaching`;

/*!50001 DROP VIEW IF EXISTS `vcoaching` */;
/*!50001 DROP TABLE IF EXISTS `vcoaching` */;

/*!50001 CREATE TABLE  `vcoaching`(
 `kode` int(11) ,
 `kode_agent` int(11) ,
 `kode_kts` int(11) ,
 `jenis_bina` int(11) ,
 `tgl_kejadian` date ,
 `tgl_bina` date ,
 `kode_dept` int(11) ,
 `kode_site` varchar(3) ,
 `masalah` text ,
 `rekomendasi` text ,
 `tgl_verifikasi` date ,
 `kode_tl` int(11) ,
 `action_plan` text ,
 `hasil_verifikasi` text ,
 `approve_agent` datetime ,
 `approve_atasan` datetime ,
 `approve_pembina` datetime ,
 `status` varchar(20) ,
 `agent` varchar(100) ,
 `tl` varchar(100) ,
 `jenis_form` varchar(10) ,
 `kts_verifikasi_str` varchar(255) ,
 `kts_subkategori` text ,
 `kts_desc` text ,
 `jenis_bina_desc` varchar(255) ,
 `kts_kategori_desc` text 
)*/;

/*Table structure for table `vkonseling` */

DROP TABLE IF EXISTS `vkonseling`;

/*!50001 DROP VIEW IF EXISTS `vkonseling` */;
/*!50001 DROP TABLE IF EXISTS `vkonseling` */;

/*!50001 CREATE TABLE  `vkonseling`(
 `kode` int(11) ,
 `kode_agent` int(11) ,
 `kode_kts` int(11) ,
 `jenis_bina` int(11) ,
 `tgl_kejadian` date ,
 `tgl_bina` date ,
 `kode_dept` int(11) ,
 `kode_site` varchar(9) ,
 `masalah` text ,
 `rekomendasi` text ,
 `tgl_verifikasi` date ,
 `kode_tl` int(11) ,
 `action_plan` text ,
 `hasil_verifikasi` text ,
 `approve_agent` datetime ,
 `approve_atasan` datetime ,
 `approve_pembina` datetime ,
 `status` varchar(60) ,
 `agent` varchar(100) ,
 `tl` varchar(100) ,
 `jenis_form` varchar(10) ,
 `kts_verifikasi_str` varchar(255) ,
 `kts_subkategori` text ,
 `kts_desc` text ,
 `jenis_bina_desc` varchar(255) ,
 `kts_kategori_desc` text 
)*/;

/*Table structure for table `vsp` */

DROP TABLE IF EXISTS `vsp`;

/*!50001 DROP VIEW IF EXISTS `vsp` */;
/*!50001 DROP TABLE IF EXISTS `vsp` */;

/*!50001 CREATE TABLE  `vsp`(
 `kode` int(11) ,
 `kode_agent` int(11) ,
 `kode_kts` int(11) ,
 `jenis_bina` int(11) ,
 `tgl_kejadian` date ,
 `tgl_bina` date ,
 `kode_dept` int(11) ,
 `kode_site` varchar(9) ,
 `masalah` text ,
 `rekomendasi` text ,
 `tgl_verifikasi` date ,
 `kode_tl` int(11) ,
 `action_plan` text ,
 `hasil_verifikasi` text ,
 `approve_agent` datetime ,
 `approve_atasan` datetime ,
 `approve_pembina` datetime ,
 `status` varchar(60) ,
 `agent` varchar(100) ,
 `tl` varchar(100) ,
 `jenis_form` varchar(10) ,
 `kts_verifikasi_str` varchar(255) ,
 `kts_subkategori` text ,
 `kts_desc` text ,
 `jenis_bina_desc` varchar(255) ,
 `kts_kategori_desc` text 
)*/;

/*View structure for view vbatl */

/*!50001 DROP TABLE IF EXISTS `vbatl` */;
/*!50001 DROP VIEW IF EXISTS `vbatl` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbatl` AS (select `t_batl`.`kode` AS `kode`,`t_batl`.`kode_agent` AS `kode_agent`,`t_batl`.`kode_kts` AS `kode_kts`,`t_batl`.`jenis_bina` AS `jenis_bina`,`t_batl`.`tgl_kejadian` AS `tgl_kejadian`,`t_batl`.`tgl_bina` AS `tgl_bina`,`t_batl`.`kode_dept` AS `kode_dept`,`t_batl`.`kode_site` AS `kode_site`,`t_batl`.`masalah` AS `masalah`,`t_batl`.`rekomendasi` AS `rekomendasi`,`t_batl`.`tgl_verifikasi` AS `tgl_verifikasi`,`t_batl`.`kode_tl` AS `kode_tl`,`t_batl`.`action_plan` AS `action_plan`,`t_batl`.`hasil_verifikasi` AS `hasil_verifikasi`,`t_batl`.`approve_agent` AS `approve_agent`,`t_batl`.`approve_atasan` AS `approve_atasan`,`t_batl`.`approve_pembina` AS `approve_pembina`,`t_batl`.`status` AS `status`,`a`.`fullname` AS `agent`,`b`.`fullname` AS `tl`,`m_kts_subkategori`.`jenis_form` AS `jenis_form`,`m_kts_subkategori`.`kts_verifikasi_str` AS `kts_verifikasi_str`,`m_kts_subkategori`.`kts_subkategori` AS `kts_subkategori`,`m_kts_subkategori`.`kts_desc` AS `kts_desc`,`m_jenis_bina`.`jenis_bina_desc` AS `jenis_bina_desc`,`m_kts_kategori`.`kts_kategori_desc` AS `kts_kategori_desc` from (((((`t_batl` join `m_users` `a` on((`a`.`kode` = `t_batl`.`kode_agent`))) join `m_users` `b` on((`b`.`kode` = `t_batl`.`kode_tl`))) join `m_kts_subkategori` on((`m_kts_subkategori`.`kode` = `t_batl`.`kode_kts`))) join `m_jenis_bina` on((`m_jenis_bina`.`kode` = `m_kts_subkategori`.`jenis_bina`))) join `m_kts_kategori` on((`m_kts_kategori`.`kode` = `m_kts_subkategori`.`kts_kategori`)))) */;

/*View structure for view vcoaching */

/*!50001 DROP TABLE IF EXISTS `vcoaching` */;
/*!50001 DROP VIEW IF EXISTS `vcoaching` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vcoaching` AS (select `t_coaching`.`kode` AS `kode`,`t_coaching`.`kode_agent` AS `kode_agent`,`t_coaching`.`kode_kts` AS `kode_kts`,`t_coaching`.`jenis_bina` AS `jenis_bina`,`t_coaching`.`tgl_kejadian` AS `tgl_kejadian`,`t_coaching`.`tgl_bina` AS `tgl_bina`,`t_coaching`.`kode_dept` AS `kode_dept`,`t_coaching`.`kode_site` AS `kode_site`,`t_coaching`.`masalah` AS `masalah`,`t_coaching`.`rekomendasi` AS `rekomendasi`,`t_coaching`.`tgl_verifikasi` AS `tgl_verifikasi`,`t_coaching`.`kode_tl` AS `kode_tl`,`t_coaching`.`action_plan` AS `action_plan`,`t_coaching`.`hasil_verifikasi` AS `hasil_verifikasi`,`t_coaching`.`approve_agent` AS `approve_agent`,`t_coaching`.`approve_atasan` AS `approve_atasan`,`t_coaching`.`approve_pembina` AS `approve_pembina`,`t_coaching`.`status` AS `status`,`a`.`fullname` AS `agent`,`b`.`fullname` AS `tl`,`m_kts_subkategori`.`jenis_form` AS `jenis_form`,`m_kts_subkategori`.`kts_verifikasi_str` AS `kts_verifikasi_str`,`m_kts_subkategori`.`kts_subkategori` AS `kts_subkategori`,`m_kts_subkategori`.`kts_desc` AS `kts_desc`,`m_jenis_bina`.`jenis_bina_desc` AS `jenis_bina_desc`,`m_kts_kategori`.`kts_kategori_desc` AS `kts_kategori_desc` from (((((`t_coaching` join `m_users` `a` on((`a`.`kode` = `t_coaching`.`kode_agent`))) join `m_users` `b` on((`b`.`kode` = `t_coaching`.`kode_tl`))) join `m_kts_subkategori` on((`m_kts_subkategori`.`kode` = `t_coaching`.`kode_kts`))) join `m_jenis_bina` on((`m_jenis_bina`.`kode` = `m_kts_subkategori`.`jenis_bina`))) join `m_kts_kategori` on((`m_kts_kategori`.`kode` = `m_kts_subkategori`.`kts_kategori`)))) */;

/*View structure for view vkonseling */

/*!50001 DROP TABLE IF EXISTS `vkonseling` */;
/*!50001 DROP VIEW IF EXISTS `vkonseling` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vkonseling` AS (select `t_konseling`.`kode` AS `kode`,`t_konseling`.`kode_agent` AS `kode_agent`,`t_konseling`.`kode_kts` AS `kode_kts`,`t_konseling`.`jenis_bina` AS `jenis_bina`,`t_konseling`.`tgl_kejadian` AS `tgl_kejadian`,`t_konseling`.`tgl_bina` AS `tgl_bina`,`t_konseling`.`kode_dept` AS `kode_dept`,`t_konseling`.`kode_site` AS `kode_site`,`t_konseling`.`masalah` AS `masalah`,`t_konseling`.`rekomendasi` AS `rekomendasi`,`t_konseling`.`tgl_verifikasi` AS `tgl_verifikasi`,`t_konseling`.`kode_tl` AS `kode_tl`,`t_konseling`.`action_plan` AS `action_plan`,`t_konseling`.`hasil_verifikasi` AS `hasil_verifikasi`,`t_konseling`.`approve_agent` AS `approve_agent`,`t_konseling`.`approve_atasan` AS `approve_atasan`,`t_konseling`.`approve_pembina` AS `approve_pembina`,`t_konseling`.`status` AS `status`,`a`.`fullname` AS `agent`,`b`.`fullname` AS `tl`,`m_kts_subkategori`.`jenis_form` AS `jenis_form`,`m_kts_subkategori`.`kts_verifikasi_str` AS `kts_verifikasi_str`,`m_kts_subkategori`.`kts_subkategori` AS `kts_subkategori`,`m_kts_subkategori`.`kts_desc` AS `kts_desc`,`m_jenis_bina`.`jenis_bina_desc` AS `jenis_bina_desc`,`m_kts_kategori`.`kts_kategori_desc` AS `kts_kategori_desc` from (((((`t_konseling` join `m_users` `a` on((`a`.`kode` = `t_konseling`.`kode_agent`))) join `m_users` `b` on((`b`.`kode` = `t_konseling`.`kode_tl`))) join `m_kts_subkategori` on((`m_kts_subkategori`.`kode` = `t_konseling`.`kode_kts`))) join `m_jenis_bina` on((`m_jenis_bina`.`kode` = `m_kts_subkategori`.`jenis_bina`))) join `m_kts_kategori` on((`m_kts_kategori`.`kode` = `m_kts_subkategori`.`kts_kategori`)))) */;

/*View structure for view vsp */

/*!50001 DROP TABLE IF EXISTS `vsp` */;
/*!50001 DROP VIEW IF EXISTS `vsp` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vsp` AS (select `t_sp`.`kode` AS `kode`,`t_sp`.`kode_agent` AS `kode_agent`,`t_sp`.`kode_kts` AS `kode_kts`,`t_sp`.`jenis_bina` AS `jenis_bina`,`t_sp`.`tgl_kejadian` AS `tgl_kejadian`,`t_sp`.`tgl_bina` AS `tgl_bina`,`t_sp`.`kode_dept` AS `kode_dept`,`t_sp`.`kode_site` AS `kode_site`,`t_sp`.`masalah` AS `masalah`,`t_sp`.`rekomendasi` AS `rekomendasi`,`t_sp`.`tgl_verifikasi` AS `tgl_verifikasi`,`t_sp`.`kode_tl` AS `kode_tl`,`t_sp`.`action_plan` AS `action_plan`,`t_sp`.`hasil_verifikasi` AS `hasil_verifikasi`,`t_sp`.`approve_agent` AS `approve_agent`,`t_sp`.`approve_atasan` AS `approve_atasan`,`t_sp`.`approve_pembina` AS `approve_pembina`,`t_sp`.`status` AS `status`,`a`.`fullname` AS `agent`,`b`.`fullname` AS `tl`,`m_kts_subkategori`.`jenis_form` AS `jenis_form`,`m_kts_subkategori`.`kts_verifikasi_str` AS `kts_verifikasi_str`,`m_kts_subkategori`.`kts_subkategori` AS `kts_subkategori`,`m_kts_subkategori`.`kts_desc` AS `kts_desc`,`m_jenis_bina`.`jenis_bina_desc` AS `jenis_bina_desc`,`m_kts_kategori`.`kts_kategori_desc` AS `kts_kategori_desc` from (((((`t_sp` join `m_users` `a` on((`a`.`kode` = `t_sp`.`kode_agent`))) join `m_users` `b` on((`b`.`kode` = `t_sp`.`kode_tl`))) join `m_kts_subkategori` on((`m_kts_subkategori`.`kode` = `t_sp`.`kode_kts`))) join `m_jenis_bina` on((`m_jenis_bina`.`kode` = `m_kts_subkategori`.`jenis_bina`))) join `m_kts_kategori` on((`m_kts_kategori`.`kode` = `m_kts_subkategori`.`kts_kategori`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
